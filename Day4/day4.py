import re

f = open("input.txt")
lines = f.readlines()
f.close()

part_1_total = 0
part_2_total = 0
total_cards = {}

def add_card(card):
    if card in total_cards.keys():
        total_cards[card] += 1
    else:
        total_cards[card] = 1

for line in lines:
    line_parts = re.match(r'Card +(\d+): ([\d ]+) \| ([\d ]+)', line)

    card = int(line_parts.group(1))

    winning_nums = re.findall(r'\d+', line_parts.group(2))
    winning_nums = [int(num) for num in winning_nums]

    card_nums = re.findall(r'\d+', line_parts.group(3))
    card_nums = [int(num) for num in card_nums]

    matches = 0
    for num in card_nums:
        if num in winning_nums:
            matches += 1

    card_score = int(2**(matches - 1))
    part_1_total += card_score

    add_card(card)
    for i in range(total_cards[card]):
        for j in range(card + 1, card + matches + 1):
            add_card(j)


    print("{} copies of Card {}".format(total_cards[card], card))

print("Part 1 Total: {}".format(part_1_total))

for cards in total_cards.values():
    part_2_total += cards

print("Part 2 Total: {}".format(part_2_total))
