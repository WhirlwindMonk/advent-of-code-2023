import sys
sys.path.append('..')
import aoc

TEST_1_SOLUTIONS = [5, 4, 405]
TEST_2_SOLUTIONS = [3, 1, 400]

def separate_mazes(lines: list[str]) -> list[list[str]]:
    mazes = []
    maze = []
    for line in lines:
        if line != '':
            maze.append(line)
        else:
            mazes.append(tuple(maze))
            maze = []

    mazes.append(tuple(maze))
    return tuple(mazes)

def check_vertical_symmetry(maze, req_errors = 0) -> int:
    line = 1
    x = 0
    y = 0
    spacing = 1
    errors = 0
    while True:
        if line >= len(maze[0]):
            return 0

        if x + spacing >= len(maze[0]) or x < 0:
            if errors == req_errors:
                return line
            else:
                y = 0
                x = line
                spacing = 1
                line += 1
                errors = 0
        elif y >= len(maze):
            y = 0
            x -= 1
            spacing += 2
        elif maze[y][x] == maze[y][x + spacing]:
            y += 1
        else:
            if errors < req_errors:
                errors += 1
                y += 1
            else:
                errors = 0
                y = 0
                x = line
                spacing = 1
                line += 1

def check_horizontal_symmetry(maze, req_errors = 0) -> int:
    line = 1
    x = 0
    y = 0
    spacing = 1
    errors = 0
    while True:
        if line >= len(maze):
            return 0

        if y + spacing >= len(maze) or y < 0:
            if errors == req_errors:
                return line
            else:
                errors = 0
                x = 0
                y = line
                spacing = 1
                line += 1
        elif x >= len(maze[0]):
            x = 0
            y -= 1
            spacing += 2
        elif maze[y][x] == maze[y + spacing][x]:
            x += 1
        else:
            if errors < req_errors:
                errors += 1
                x += 1
            else:
                errors = 0
                x = 0
                y = line
                spacing = 1
                line += 1

def main():
    test_lines = aoc.get_file_lines("test1.txt")
    test_mazes = separate_mazes(test_lines)

    test_total = 0
    for i in range(len(test_mazes)):
        x = check_vertical_symmetry(test_mazes[i])
        y = check_horizontal_symmetry(test_mazes[i])

        if x:
            assert x == TEST_1_SOLUTIONS[i], "Error: Generated value of {} does not match solution of {}!".format(x, TEST_1_SOLUTIONS[i])
        else:
            assert y == TEST_1_SOLUTIONS[i], "Error: Generated value of {} does not match solution of {}!".format(y, TEST_1_SOLUTIONS[i])

        test_total += x
        test_total += (100 * y)

    assert test_total == TEST_1_SOLUTIONS[-1], "Error: Generated value of {} does not match solution of {}!".format(test_total, TEST_1_SOLUTIONS[-1])

    input_file = aoc.get_input_data()
    lines = aoc.get_file_lines(input_file)
    mazes = separate_mazes(lines)

    part_1_total = 0
    for maze in mazes:
        x = check_vertical_symmetry(maze)
        part_1_total += x

        y = check_horizontal_symmetry(maze)
        part_1_total += (100 * y)

    aoc.print_solution(1, part_1_total)

    test_total = 0
    for i in range(len(test_mazes)):
        x = check_vertical_symmetry(test_mazes[i], 1)
        y = check_horizontal_symmetry(test_mazes[i], 1)

        if x:
            assert x == TEST_2_SOLUTIONS[i], "Error: Generated value of {} does not match solution of {}!".format(x, TEST_2_SOLUTIONS[i])
        else:
            assert y == TEST_2_SOLUTIONS[i], "Error: Generated value of {} does not match solution of {}!".format(y, TEST_2_SOLUTIONS[i])

        test_total += x
        test_total += (100 * y)

    assert test_total == TEST_2_SOLUTIONS[-1], "Error: Generated value of {} does not match solution of {}!".format(test_total, TEST_2_SOLUTIONS[-1])

    part_2_total = 0
    for maze in mazes:
        x = check_vertical_symmetry(maze, 1)
        part_2_total += x

        y = check_horizontal_symmetry(maze, 1)
        part_2_total += (100 * y)

    aoc.print_solution(2, part_2_total)

if __name__ == '__main__':
    main()
