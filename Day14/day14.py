import sys, itertools, time
sys.path.append('..')
import aoc

from enum import Enum

obstacles = []

TEST_1_SOLUTION = 136
TEST_2_SOLUTION = 64

direction = {
    "Up": (1, 0),
    "Left": (0, 1),
    "Down": (-1, 0),
    "Right": (0, -1)
    }

def process_lines(lines: list[str]) -> list[tuple[int, int]]:
    rocks_init = []
    for line in range(len(lines)):
        for char in range(len(lines[0])):
            if lines[line][char] == "O":
                rocks_init.append((line, char))
            elif lines[line][char] == "#":
                obstacles.append((line, char))

    return rocks_init

def tilt_platform(r: list[tuple[int, int]], d: tuple[int, int], size: tuple[int, int]) -> list[tuple[int, int]]:
    rocks_moved = []
    o_temp = obstacles[:]

    r = list(r)
    r.sort(key=lambda x: x[0] * d[0] + x[1] * d[1])

    for rock in r:
        if (d == direction["Up"] and rock[0] == 0) or (d == direction["Left"] and rock[1] == 0) or (d == direction["Down"] and rock[0] == size[0] - 1) or (d == direction["Right"] and rock[1] == size[1] - 1):
            new_rock = rock
        else:
            in_the_way = list(itertools.filterfalse(lambda x: x[abs(d[0])] != rock[abs(d[0])] or x[0] * d[0] + x[1] * d[1] > rock[0] * d[0] + rock[1] * d[1], o_temp))

            if d[0]:
                if len(in_the_way) == 0:
                    if d[0] == 1:
                        row = 0
                    else:
                        row = size[0] - 1

                else:
                    row = max(in_the_way, key=lambda x: x[0] * d[0])[0] + d[0]

                col = rock[1]
            else:
                if len(in_the_way) == 0:
                    if d[1] == 1:
                        col = 0
                    else:
                        col = size[1] - 1
                else:
                    col = max(in_the_way, key=lambda x: x[1] * d[1])[1] + d[1]

                row = rock[0]

            new_rock = (row, col)

        rocks_moved.append(new_rock)
        o_temp.append(new_rock)

    return rocks_moved

def calculate_load(r: list[tuple[int, int]], h: int) -> int:
    total = 0
    for rock in r:
        total += (h - rock[0])

    return total

def print_grid(r, o, s) -> None:
    for row in range(s):
        row_str = ""
        for col in range(s):
            if (row, col) in r:
                row_str += "O"
            elif (row, col) in o:
                row_str += "#"
            else:
                row_str += "."
        print(row_str)
    print()

def perform_part_2_cycles(r: list[tuple[int, int]], size: tuple[int, int]):
    cache = {}

    for i in range(1000000000):
        for d in direction.values():
            r = tilt_platform(r, d, size)

        if r not in cache.values():
            cache[i] = r
        else:
            prev_i = list(cache.keys())[list(cache.values()).index(r)]
            cycle = i - prev_i
            remaining = (1000000000 - prev_i) % cycle
            return cache[prev_i + remaining - 1]

def main() -> None:
    if __debug__:
        test_1_lines = aoc.get_file_lines("test1.txt")
        test_platform_size = (len(test_1_lines), len(test_1_lines[0]))
        test_r = process_lines(test_1_lines)
        test_1_r = tilt_platform(test_r, direction["Up"], test_platform_size)
        test_1_solution = calculate_load(test_1_r, test_platform_size[0])

        aoc.test_assertion(1, test_1_solution, TEST_1_SOLUTION)

        test_r = perform_part_2_cycles(test_r, test_platform_size)
        test_2_solution = calculate_load(test_r, test_platform_size[0])

        aoc.test_assertion(2, test_2_solution, TEST_2_SOLUTION)
        return

    input_file = aoc.get_input_data()
    input_lines = aoc.get_file_lines(input_file)
    platform_size = (len(input_lines), len(input_lines[0]))
    r = process_lines(input_lines)

    part_1_r = tilt_platform(r, direction["Up"], platform_size)
    part_1_solution = calculate_load(part_1_r, platform_size[0])

    aoc.print_solution(1, part_1_solution)

    r = perform_part_2_cycles(r, platform_size)
    part_2_solution = calculate_load(r, platform_size[0])

    aoc.print_solution(2, part_2_solution)

if __name__ == '__main__':
    main()
