import re

f = open("input.txt", "r")

lines = f.readlines()

limits = { "red": 12, "green": 13, "blue": 14 }
valid_total = 0
power_total = 0

for line in lines:
    regex = re.match(r'Game (\d+)\: (.+)', line)
    game = int(regex.group(1))
    results = regex.group(2)
    matches = re.findall(r'(\d+) (\w+)', results)

    line_max = {}
    for match in matches:
        if match[1] in line_max.keys():
            line_max[match[1]] = int(match[0]) if int(match[0]) > line_max[match[1]] else line_max[match[1]]
        else:
            line_max[match[1]] = int(match[0])

    valid = True
    for color in limits.keys():
        if color in line_max.keys():
            if line_max[color] > limits[color]:
                valid = False
                break

    power = 1
    for value in line_max.values():
        power *= value
    power_total += power
#    print(str(game) + " - " + str(line_max))
#    print("valid" if valid else "invalid")
    if valid:
        valid_total += game

print(valid_total)
print(power_total)
