import sys, itertools, time
sys.path.append('..')
import aoc

TEST_1_SOLUTION = 46
TEST_2_SOLUTION = 51

visited = {}

def process_lines(lines: list[str]) -> list[list[str]]:
    mapped_lines = map(list, lines)
    return list(mapped_lines)

def solve_part_1(data: list[str]) -> int:
    visited.clear()
    grid = process_lines(data)
    chart_path(grid, (0, 0), (0, 1))
    sol = len(visited.keys())
    return sol

def solve_part_2(data: list[str]) -> int:
    grid = process_lines(data)
    last_row = len(data) - 1
    last_col = len(data[0]) - 1
    max_count = 0
    for row in range(len(data)):
        visited.clear()
        chart_path(grid, (row, 0), (0, 1))
        max_count = max(max_count, len(visited.keys()))
        visited.clear()
        chart_path(grid, (row, last_col), (0, -1))

    for col in range(len(data[0])):
        visited.clear()
        chart_path(grid, (0, col), (1, 0))
        max_count = max(max_count, len(visited.keys()))
        visited.clear()
        chart_path(grid, (last_row, col), (-1, 0))

    return max_count

def chart_path(grid: list[list[str]], s: tuple[int, int], d: tuple[int, int]) -> None:
    row, col = s
    count = 0
    while True:
        if row < 0 or col < 0 or row >= len(grid) or col >= len(grid[0]):
            return

        if (row, col) not in visited.keys():
            visited[(row, col)] = d
        elif visited[(row, col)] == d:
            return

        match grid[row][col]:
            case '.':
                pass
            case '\\':
                d = (d[1], d[0])
            case '/':
                d = (-d[1], -d[0])
            case '-':
                if d[0] != 0:
                    chart_path(grid, (row, col + 1), (0, 1))
                    chart_path(grid, (row, col - 1), (0, -1))
                    return
            case '|':
                if d[1] != 0:
                    chart_path(grid, (row + 1, col), (1, 0))
                    chart_path(grid, (row - 1, col), (-1, 0))
                    return

        row, col = aoc.vector_add((row, col), d)

def main() -> None:
    if __debug__:
        test_data = aoc.get_file_lines("test1.txt")
        test_1_solution = solve_part_1(test_data)
        aoc.test_assertion(1, test_1_solution, TEST_1_SOLUTION)

        test_2_solution = solve_part_2(test_data)
        aoc.test_assertion(2, test_2_solution, TEST_2_SOLUTION)

    input_file = aoc.get_input_file()
    input_data = aoc.get_file_lines(input_file)
    part_1_solution = solve_part_1(input_data)
    aoc.print_solution(1, part_1_solution)

    part_2_solution = solve_part_2(input_data)
    aoc.print_solution(2, part_2_solution)

if __name__ == '__main__':
    main()
