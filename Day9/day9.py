import re, collections, itertools

f = open("input.txt", "r")
lines = f.readlines()
f.close()

lines = map(lambda x: x.split(), lines)

part_1_total = 0
part_2_total = 0
for line in lines:
    top_level = list(map(int, line))
    difs = []
    difs.append(top_level)
    # print(top_level)
    all_zeros = False
    while not all_zeros:
        zips = itertools.zip_longest(top_level[1:], top_level[:-1])
        next_level = list(map(lambda x: x[0] - x[1], zips))
        difs.append(next_level)
        top_level = next_level

        if top_level.count(0) == len(top_level):
            all_zeros = True

    difs.reverse()

    end_value = 0
    start_value = 0
    for line in difs:
        end_value += line[-1]
        start_value = line[0] - start_value

    part_1_total += end_value
    part_2_total += start_value

print("Part 1 solution: {}".format(part_1_total))
print("Part 2 solution: {}".format(part_2_total))
