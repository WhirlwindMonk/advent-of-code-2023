import re
from collections import Counter

f = open("input.txt", "r")
lines = f.readlines()
f.close()

def part_one_sort_key(play):
    hand, bid = play
    cards = re.findall(r'\w', hand)
    card_counts = Counter(cards)

    value_list = list(card_counts.values())

    score = 0
    if max(value_list) == 5:
        # print("{}: Five of a kind".format(hand))
        score += 70000000000
    elif max(value_list) == 4:
        # print("{}: Four of a kind".format(hand))
        score += 60000000000
    elif max(value_list) == 3 and min(value_list) == 2:
        # print("{}: Full house".format(hand))
        score += 50000000000
    elif max(value_list) == 3 and min(value_list) == 1:
        # print("{}: Three of a kind".format(hand))
        score += 40000000000
    elif value_list.count(2) == 2:
        # print("{}: Two pair".format(hand))
        score += 30000000000
    elif value_list.count(1) == 3:
        # print("{}: One pair".format(hand))
        score += 20000000000
    else:
        # print("{}: High card".format(hand))
        score += 10000000000

    for i in range(len(cards)):
        if cards[i] in "23456789":
            card_score = int(cards[i])
        else:
            match cards[i]:
                case "T":
                    card_score = 10
                case "J":
                    card_score = 11
                case "Q":
                    card_score = 12
                case "K":
                    card_score = 13
                case "A":
                    card_score = 14
        score += card_score * (10 ** (8 - 2 * i))

    # print(score)
    return score

def part_two_sort_key(play):
    hand, score = play
    cards = re.findall(r'\w', hand)
    card_counts = Counter(cards)

    if "J" in card_counts.keys():
        j_count = card_counts.pop("J")
        value_list = list(card_counts.values())
        if j_count == 5:
            card_counts[2] = 5
        else:
            for card in card_counts.keys():
                if card_counts[card] == max(value_list):
                    card_counts[card] += j_count
                    break

    # print(card_counts)
    value_list = list(card_counts.values())
    score = 0
    if max(value_list) == 5:
        # print("{}: Five of a kind".format(hand))
        score += 70000000000
    elif max(value_list) == 4:
        # print("{}: Four of a kind".format(hand))
        score += 60000000000
    elif max(value_list) == 3 and min(value_list) == 2:
        # print("{}: Full house".format(hand))
        score += 50000000000
    elif max(value_list) == 3 and min(value_list) == 1:
        # print("{}: Three of a kind".format(hand))
        score += 40000000000
    elif value_list.count(2) == 2:
        # print("{}: Two pair".format(hand))
        score += 30000000000
    elif value_list.count(1) == 3:
        # print("{}: One pair".format(hand))
        score += 20000000000
    else:
        # print("{}: High card".format(hand))
        score += 10000000000

    for i in range(len(cards)):
        if cards[i] in "23456789":
            card_score = int(cards[i])
        else:
            match cards[i]:
                case "T":
                    card_score = 10
                case "J":
                    card_score = 1
                case "Q":
                    card_score = 12
                case "K":
                    card_score = 13
                case "A":
                    card_score = 14
        score += card_score * (10 ** (8 - 2 * i))

    # print("{}: {}".format(hand, score))
    return score


hands = []
for line in lines:
    hand, bid = line.split()
    hands.append((hand, int(bid)))

hands.sort(key=part_one_sort_key)

total_score = 0
for i in range(len(hands)):
    total_score += hands[i][1] * (i + 1)

print("Part 1 solution: {}".format(total_score))

hands.sort(key=part_two_sort_key)

# print(hands)

total_score = 0
for i in range(len(hands)):
    total_score += hands[i][1] * (i + 1)

print("Part 2 solution: {}".format(total_score))
