import re

# f = open("demo.txt", "r")
f = open("input.txt", "r")

lines = f.readlines()
f.close()

total = 0

data = []
rows = len(lines)
cols = len(lines[0]) - 1
for line in lines:
    line_data = list(line)
    line_data = line_data[:-1]
    data.append(line_data)

gear_total = 0
gears = {}

skips = 0
for i in range(rows):
    for j in range(cols):
        if skips > 0:
            skips -= 1
        else:
            if data[i][j] in "0123456789":
                num = data[i][j]
                for k in range(j+1, cols):
                    if data[i][k] in "0123456789":
                        num += data[i][k]
                    else:
                        break
                skips = len(num) - 1

                prow = max(0, i - 1)
                nrow = min(rows, i + 2)
                pcol = max(0, j - 1)
                ncol = min(cols, k + 1)

                part_num = False

                debug = []
                for m in range(prow, nrow):
                    debug_row = []
                    for n in range(pcol, ncol):
                        debug_row.append(data[m][n])
                        if data[m][n] not in "0123456789.":
                            part_num = True
                            if data[m][n] == "*":
                                if (m, n) in gears:
                                    gears[(m,n)].append(int(num))
                                else:
                                    gears[(m,n)] = [int(num)]
                    if part_num:
                        pass
                    debug.append(debug_row)

                if part_num:
                    # print("{} - {} - {} - {}: {}".format(data[prow][pcol], data[prow][ncol - 1], data[nrow - 1][pcol], data[nrow - 1][ncol - 1], num))
                    total += int(num)
                    # print(debug)
                    # input()

print("Part 1 Total: {}".format(total))

for gear in gears.values():
    if len(gear) == 2:
        ratio = gear[0] * gear[1]
        gear_total += ratio

print("Part 2 Total: {}".format(gear_total))
