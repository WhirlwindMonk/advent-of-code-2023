import re

f = open("input.txt")
lines = f.readlines()

total = 0
spelled_nums = {"one": "1", "two": "2", "three": "3", "four": "4", "five": "5", "six": "6", "seven": "7", "eight": "8", "nine": "9"}

for line in lines:
    line_dict = {}
    for spelled_num in spelled_nums.keys():
        locs = [m.start() for m in re.finditer(spelled_num, line)]
        if len(locs) > 0:
            for loc in locs:
                line_dict[loc] = spelled_nums[spelled_num]
    for i in range(len(line)):
        if line[i] in "0123456789":
            line_dict[i] = line[i]
    min_loc = min(line_dict.keys())
    max_loc = max(line_dict.keys())
    coord = int(line_dict[min_loc] + line_dict[max_loc])
    print(str(line_dict) + " - " + str(coord))
    total += coord
print(total)