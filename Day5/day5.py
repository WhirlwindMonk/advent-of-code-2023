import re
import time
import math

f = open("input.txt", "r")
lines = f.readlines()
f.close()

seeds_line = lines.pop(0)
seeds_text = re.match(r'seeds: +([\d ]+)', seeds_line).group(1)
seeds_list = re.findall(r'\d+', seeds_text)
seeds_list = [int(seed) for seed in seeds_list]

next_line = ""
while next_line != "seed-to-soil map:":
    next_line = lines.pop(0).strip()

def map_value(value, step):
    for mapping in mappings[step]:
        if value >= mapping[0] and value <= mapping[1]:
            return mapping[2] + value - mapping[0]
    return value

header_regex = r'.+map:'
values_regex = r'(\d+)\s+(\d+)\s+(\d+)'

mappings = []
mapping_array = []
while len(lines) > 0:
    line = lines.pop(0)
    header_match = re.match(header_regex, line)
    values_match = re.match(values_regex, line)

    if header_match != None:
        if len(mapping_array) > 0:
            mappings.append(mapping_array)
        mapping_array = []
    elif values_match != None:
        values_list = [int(x) for x in values_match.groups()]
        mapping_tuple = (values_list[1], values_list[1] + values_list[2] - 1, values_list[0])
        mapping_array.append(mapping_tuple)
mappings.append(mapping_array)

locs = []
for seed in seeds_list:
    next_value = seed
    for i in range(len(mappings)):
        next_value = map_value(next_value, i)
    locs.append(next_value)

print("Part 1 Solution: {}".format(min(locs)))

start = time.mktime(time.gmtime())

seed_tuples = []
total_seeds = 0
for i in range(int(len(seeds_list) / 2)):
    j = 2 * i
    seed_tuple = (seeds_list[j], seeds_list[j] + seeds_list[j + 1])
    seed_tuples.append(seed_tuple)
    total_seeds += seeds_list[j + 1]

print("{} total seeds to check".format(total_seeds))

seeds_checked = 0
loc = -1
for seed_tuple in seed_tuples:
    for i in range(seed_tuple[0], seed_tuple[1]):
        next_value = i
        for j in range(len(mappings)):
            next_value = map_value(next_value, j)

        if loc == -1:
            loc = next_value
        elif next_value < loc:
            loc = next_value

        seeds_checked += 1

        if seeds_checked % 100000 == 0:
            cur_time = time.mktime(time.gmtime())
            time_passed = cur_time - start
            time_remaining = time_passed * (total_seeds - seeds_checked) / seeds_checked
            min_remaining = int(math.floor(time_remaining / 60))
            sec_remaining = int(time_remaining % 60)
            print("{}/{} seeds checked ({:.2%}). Approximately {}:{:02d} remaining.".format(seeds_checked, total_seeds, seeds_checked / total_seeds, min_remaining, sec_remaining))




print("Part 2 Solution: {}".format(loc))

end = time.mktime(time.gmtime())
print("Processing took {} seconds".format(end - start))
