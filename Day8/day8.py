import re, itertools, math

def find_steps_to_dest(dirs_string, maps, start = "AAA", end_pattern = "ZZZ", step = 0):
    loc = start

    matched = False

    while not matched:
        dirs = itertools.chain(dirs_string)
        for dir in dirs:
            if dir == "L":
                loc = maps[loc][0]
            else:
                loc = maps[loc][1]
            step += 1
        if re.match(end_pattern, loc):
            matched = True

    return (loc, step)

def prime_factor(num):
    primes = []
    while num % 2 == 0:
        primes.append(2)
        num = num / 2

    for i in range(3, int(math.sqrt(num) + 1), 2):
        while num % i == 0:
            primes.append(i)
            num = num / i

    if num > 2:
        primes.append(num)

    return primes

f = open("input.txt", "r")
lines = f.readlines()
f.close()

dirs_string = lines.pop(0).strip()

lines.pop(0)

lines = map(lambda x: re.findall(r'\w{3}', x), lines)
lines = map(lambda x: (x[0], (x[1], x[2])), lines)
maps = dict(lines)

loc, step = find_steps_to_dest(dirs_string, maps)
print("Part 1 solution: {}".format(step))

keys = maps.keys()
regex = re.compile("..A")
starts = list(filter(regex.match, keys))
starts.sort()
start_pairs = zip(starts[0:len(starts) - 1], starts[1:len(starts)])

locs = []
for pair in start_pairs:
    step_dict = dict(zip(pair, [0, 0]))
    loc_dict = dict(zip(pair, pair))
    found = False

    while not found:
        for start in pair:
            loc_dict[start], step_dict[start] = find_steps_to_dest(dirs_string, maps, loc_dict[start], r'..Z', step_dict[start])

            while step_dict[start] < max(step_dict.values()):
                loc_dict[start], step_dict[start] = find_steps_to_dest(dirs_string, maps, loc_dict[start], r'..Z', step_dict[start])

            if step_dict[pair[0]] == step_dict[pair[1]] != 0:
                found = True
                break

    locs.append(step_dict[pair[0]])

all_primes = list(map(prime_factor, locs))
final_primes = []
while len(all_primes) > 0:
    loc_primes = all_primes.pop(0)
    while len(loc_primes) > 0:
        prime = loc_primes.pop(0)
        final_primes.append(prime)
        for i in range(len(all_primes)):
            if prime in all_primes[i]:
                all_primes[i].remove(prime)

part_2_solution = 1
for prime in final_primes:
    part_2_solution *= prime

print("Part 2 solution: {}".format(part_2_solution))
