f = open("input.txt", "r")
lines = f.readlines()
f.close()

times = list(map(int, lines[0][10:].split()))
dists = list(map(int, lines[1][10:].split()))

part_1_total = 1
for i in range(len(times)):
    count = 0
    for j in range(times[i]):
        if j * (times[i] - j) > dists[i]:
            count += 1
    part_1_total *= count

print("Part 1 Solution: {}".format(part_1_total))

time = int(''.join(lines[0][10:].split()))
dist = int(''.join(lines[1][10:].split()))

count = 0
for i in range(time):
    if i * (time - i) > dist:
        count += 1
    if i * (time - i) < dist and count > 0:
        break

print("Part 2 Solution: {}".format(count))
