import itertools

DEMO_SOLUTION = 8
DEMO_2_SOLUTION = 4
DEMO_3_SOLUTION = 10
DEMO_4_SOLUTION = 8

PIPES = {
    '|': (0, 0),
    '-': (0, 0),
    'L': (1, -1),
    'J': (-1, -1),
    '7': (-1, 1),
    'F': (1, 1)
    }

# Open the file and turn it into a list of the lines in the file
def read_file(file_name: str) -> list[str]:
    f = open(file_name, "r")
    lines = f.readlines()
    f.close()
    return lines

def process_lines(lines: list[str]) -> list[list[str]]:
    # First, split the strings into individual characters, then turn the list of iterators into a list of lists
    split_lines = map(itertools.chain, lines)
    return list(map(list, split_lines))

# Returns the new coorinates in the field after moving in the given direction
def move_dir(start: tuple, direction: tuple) -> tuple:
    # Add the direction to the current coordinates to get the new coordinates
    return tuple(map(lambda x: x[0] + x[1], zip(start, direction)))

# Takes the direction moved into the tile and the tile to determine the new movement direction
def get_dir(previous_dir: tuple, tile: tuple) -> tuple:
    # Add the current direction to the tile path to get the new travel direction
    return tuple(map(lambda x: x[0] + x[1], zip(previous_dir, PIPES[tile])))

# Returns the tile in the field at the given location
def get_tile(field: list[list[str]], location: tuple) -> str:
    return field[location[1]][location[0]]

# Takes the field and finds the loop
def find_path(field) -> list[tuple]:
    path = []

    # Iterate through the lines to find the starting point
    for i in range(len(field)):
        if "S" in field[i]:
            path.append((field[i].index("S"), i))
            break

    # Check the cardinal directions around the start for appropriate pipes in order to find the loop
    for direction in [(1, 0), (-1, 0), (0, 1), (0, -1)]:
        new_loc = move_dir(path[0], direction)

        try:
            new_tile = field[new_loc[1]][new_loc[0]]
        except:
            continue
        last_dir = (0, 0)

        legal = False

        # Right
        if direction == (1, 0) and new_tile in "-J7":
            legal = True

        # Left
        elif direction == (-1, 0) and new_tile in "-LF":
            legal = True

        # Up
        elif direction == (0, -1) and new_tile in "|F7":
            legal = True

        # Down
        elif direction == (0, 1) and new_tile in "|LJ":
            legal = True

        # If a legal pipe connection is found, add the new location to the path, set the direction, and break out of the loop, we only need one of the legal pipes
        if legal:
            new_dir = direction
            break

    # Loop until we're back at the start
    while new_loc != path[0]:
        prev_dir = new_dir
        cur_loc = new_loc
        path.append(cur_loc)

        tile = get_tile(field, cur_loc)

        new_dir = get_dir(prev_dir, tile)
        new_loc = move_dir(cur_loc, new_dir)

    return path

def calc_part_1_solution(path: list[tuple]) -> int:
    return int(len(path) / 2)

def combine_turns(tile_1: str, tile_2: str):
    return tuple(map(lambda x: x[0] + x[1], zip(PIPES[tile_1], PIPES[tile_2])))

def find_part_2_solution(field: list[list[str]], path: list[tuple]) -> int:
    row_count = len(field)
    col_count = len(field[0])

    for row in range(row_count):
        for col in range(col_count):
            if (col, row) not in path:
                field[row][col] = '.'

    enclosed_list = []
    for row in range(row_count):
        wall_count = 0
        prev_wall = ''
        for col in range(col_count):
            tile = field[row][col]
            if tile == '.' and wall_count % 2 == 1:
                enclosed_list.append((col, row))
            elif tile == '|':
                wall_count += 1
            elif tile in "FJL7":
                if prev_wall == '':
                    prev_wall = tile
                else:
                    if combine_turns(prev_wall, tile) == (0, 0):
                        wall_count += 1
                    prev_wall = ''

    return len(enclosed_list)

def main() -> bool:
    if __debug__:
        # Test the code first
        demo_lines = read_file("demo.txt")
        processed_demo_lines = process_lines(demo_lines)
        path = find_path(processed_demo_lines)
        sol = calc_part_1_solution(path)
        assert sol == DEMO_SOLUTION, "Demo test failed, output was {} when it should be {}".format(sol, DEMO_SOLUTION)

    input_lines = read_file("input.txt")
    processed_lines = process_lines(input_lines)
    path = find_path(processed_lines)
    part_1_solution = calc_part_1_solution(path)

    print("Part 1 solution: {}".format(part_1_solution))

    if __debug__:
        # Test the code first
        for file_name in ["demo2.txt", "demo3.txt", "demo4.txt"]:
            demo_lines = read_file(file_name)
            processed_demo_lines = process_lines(demo_lines)
            path = find_path(processed_demo_lines)
            sol = find_part_2_solution(processed_demo_lines, path)
            if "2" in file_name:
                assert sol == DEMO_2_SOLUTION, "Demo 2 test failed, output was {} when it should be {}".format(sol, DEMO_2_SOLUTION)
            elif "3" in file_name:
                assert sol == DEMO_3_SOLUTION, "Demo 3 test failed, output was {} when it should be {}".format(sol, DEMO_3_SOLUTION)
            else:
                assert sol == DEMO_4_SOLUTION, "Demo 4 test failed, output was {} when it should be {}".format(sol, DEMO_4_SOLUTION)

    part_2_solution = find_part_2_solution(processed_lines, path)
    print("Part 2 solution: {}".format(part_2_solution))

if __name__ == '__main__':
    main()
