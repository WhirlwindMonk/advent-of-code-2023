import sys, itertools, time
sys.path.append('..')
import aoc

TEST_1_SOLUTION = 1320
TEST_2_SOLUTION = 145

def get_steps(test_data: list[str]) -> list[str]:
    return test_data[0].split(',')

def hash_algo(text: str) -> int:
    hash_total = 0
    for char in text:
        ascii_code = ord(char)
        hash_total += ascii_code
        hash_total *= 17
        hash_total %= 256

    return hash_total

def solve_part_1(steps: list[str]) -> int:
    solution = 0
    for step in steps:
        solution += hash_algo(step)

    return solution

def solve_part_2(steps: list[str]) -> int:
    lens_boxes = {}
    for step in steps:
        if "=" in step:
            label, focal_length = step.split('=')
            focal_length = int(focal_length)
            box = hash_algo(label)
            if box in lens_boxes.keys():
                label_exists = False
                for i, lens in enumerate(lens_boxes[box]):
                    if label in lens:
                        label_exists = True
                        lens_boxes[box][i][1] = focal_length

                if not label_exists:
                    lens_boxes[box].append([label, focal_length])
            else:
                lens_boxes[box] = [[label, focal_length]]
        elif "-" in step:
            label = step[:-1]
            box = hash_algo(label)
            if box in lens_boxes.keys():
                for lens in lens_boxes[box]:
                    if label in lens:
                        lens_boxes[box].remove(lens)

    solution = 0
    for box in lens_boxes.keys():
        for i, lens in enumerate(lens_boxes[box]):
            solution += (box + 1) * (i + 1) * (lens_boxes[box][i][1])

    return solution

def main() -> None:
    if __debug__:
        test_data = aoc.get_file_lines("test1.txt")
        test_steps = get_steps(test_data)
        test_1_solution = solve_part_1(test_steps)
        aoc.test_assertion(1, test_1_solution, TEST_1_SOLUTION)

        test_2_solution = solve_part_2(test_steps)
        aoc.test_assertion(2, test_2_solution, TEST_2_SOLUTION)

    input_file = aoc.get_input_file()
    input_data = aoc.get_file_lines(input_file)
    input_steps = get_steps(input_data)
    part_1_solution = solve_part_1(input_steps)
    aoc.print_solution(1, part_1_solution)

    part_2_solution = solve_part_2(input_steps)
    aoc.print_solution(2, part_2_solution)

if __name__ == '__main__':
    main()
