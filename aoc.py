import requests, re, math, time

ns_times = []

def get_file_lines(filename: str) -> list[str]:
    ns_times.append(get_ns_time())
    with open(filename, "r") as f:
        lines = f.readlines()

    for i in range(len(lines)):
        lines[i] = lines[i].strip()

    return lines

def get_input_data() -> str:
    return get_input_file()

def get_input_file() -> str:
    day = int(re.match(r'.+?(\d+).+', __file__).group(1))

    filename = "input.txt"
    try:
        input_file = open("/home/adam/Documents/Dev/AdventOfCode/Day{}/{}".format(day, filename), "r")
    except:
        input_file = open("/home/adam/Documents/Dev/AdventOfCode/Day{}/{}".format(day, filename), "w")
    else:
        input_file.close()
        return filename

    with open("/home/adam/Documents/Dev/AdventOfCode/session.txt") as session_file:
        session_key = session_file.readline().strip()

    url = "https://adventofcode.com/2023/day/{}/input".format(day)
    cookies = {'session': session_key}
    r = requests.get(url, cookies=cookies)
    input_file.write(r.text)
    input_file.close()
    return filename

def print_solution(part: int, sol: int, time = 0) -> None:
    if not time:
        ns_times.append(get_ns_time())

        time = ns_times[-1] - ns_times[-2]

    s = time / 1000000000

    minutes = int(math.floor(s / 60))
    seconds = s - (minutes * 60.0)


    if time > 0:
        print("Part {} solution: {} (found in {}:{:05.2f})".format(part, sol, minutes, seconds))
    else:
        print("Part {} solution: {}".format(part, sol))

def test_assertion(test: int, my_sol: int, real_sol: int) -> None:
    assert my_sol == real_sol, "Error: Generated test {} value of {} does not match solution of {}".format(test, my_sol, real_sol)

def get_ns_time() -> int:
    return time.clock_gettime_ns(time.CLOCK_REALTIME)

def vector_add(a: tuple, b: tuple) -> tuple:
    assert len(a) == len(b), "Cannot sum vectors of different lengths"
    c = zip(a, b)
    return tuple(map(lambda x: x[0] + x[1], c))
