import itertools

DEMO_1_SOLUTION = 374
PART_1_SOLUTION = 9329143
TEST_2_SOLUTION = 1030
TEST_3_SOLUTION = 8410

def get_file_data(filename: str) -> list[str]:
    f = open(filename, "r")
    lines = f.readlines()
    f.close()

    for i in range(len(lines)):
        lines[i] = lines[i].strip()

    return lines

def get_star_chart(data: list[str]) -> list[tuple[int, int]]:
    rows = len(data)
    cols = len(data[0])

    star_chart = []
    for row in range(rows):
        for col in range(cols):
            if data[row][col] == "#":
                star_chart.append((col, row))

    return star_chart

def get_expansion_data(star_chart: list[tuple], data: list[tuple[int, int]]) -> tuple[list[int], list[int]]:
    rows = len(data)
    cols = len(data[0])

    galaxy_rows = list(map(lambda x: x[1], star_chart))
    galaxy_cols = list(map(lambda x: x[0], star_chart))

    empty_rows = list(itertools.filterfalse(lambda x: x in galaxy_rows, range(rows)))
    empty_cols = list(itertools.filterfalse(lambda x: x in galaxy_cols, range(cols)))

    return (empty_cols, empty_rows)

def calculate_solution(star_chart, expansion_data, expansion_rate = 1) -> int:
    pairs = itertools.combinations(star_chart, 2)

    empty_cols, empty_rows = expansion_data

    total_distance = 0
    for g1, g2 in pairs:
        x_dist = abs(g2[0] - g1[0])
        y_dist = abs(g2[1] - g1[1])

        for row in empty_rows:
            if row > min(g2[1], g1[1]) and row < max(g2[1], g1[1]):
                y_dist += expansion_rate

        for col in empty_cols:
            if col > min(g2[0], g1[0]) and col < max(g2[0], g1[0]):
                x_dist += expansion_rate

        total_distance += (x_dist + y_dist)

    return total_distance

def main() -> None:
    demo_data = get_file_data("demo1.txt")
    demo_star_chart = get_star_chart(demo_data)
    demo_expansion_data = get_expansion_data(demo_star_chart, demo_data)
    demo_solution = calculate_solution(demo_star_chart, demo_expansion_data)

    assert demo_solution == DEMO_1_SOLUTION, "Test 1 failed, {} does not match actual solution of {}".format(demo_solution, DEMO_1_SOLUTION)

    data = get_file_data("input.txt")
    star_chart = get_star_chart(data)
    expansion_data = get_expansion_data(star_chart, data)
    part_1_solution = calculate_solution(star_chart, expansion_data)

    assert part_1_solution == PART_1_SOLUTION, "Error, part 1 now returning incorrect answer"
    print("Part 1 solution: {}".format(part_1_solution))

    test_2 = calculate_solution(demo_star_chart, demo_expansion_data, 9)
    test_3 = calculate_solution(demo_star_chart, demo_expansion_data, 99)

    assert test_2 == TEST_2_SOLUTION, "Test 2 failed, {} does not match actual solution of {}".format(test_2, TEST_2_SOLUTION)
    assert test_3 == TEST_3_SOLUTION, "Test 3 failed, {} does not match actual solution of {}".format(test_3, TEST_3_SOLUTION)

    part_2_solution = calculate_solution(star_chart, expansion_data, 999999)

    print("Part 2 solution: {}".format(part_2_solution))

if __name__ == '__main__':
    main()
