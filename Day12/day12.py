import re, itertools
from functools import cache

TEST_1_SOLUTION = 21
TEST_2_SOLUTION = 525152

def get_file_data(filename: str) -> list[str]:
    f = open(filename, "r")
    lines = f.readlines()
    f.close()

    for i in range(len(lines)):
        lines[i] = lines[i].strip()

    return lines

def process_line(line: str, part2 = False) -> tuple[str, list[int]]:
    springs, groups = line.split()
    if part2:
        springs = '?'.join(itertools.repeat(springs, 5))
        groups = ','.join(itertools.repeat(groups, 5))
    springs += "."
    groups = tuple(map(int, groups.split(',')))
    return springs, groups

@cache
def process_springs(p, g, springs, groups) -> int:
    if g >= len(groups):
        if p < len(springs) and "#" in springs[p:]:
            return 0
        return 1

    if p >= len(springs):
        return 0

    if springs[p] == "?":
        if "." not in springs[p:p + groups[g]] and springs[p + groups[g]] != "#":
            result = process_springs(p + groups[g] + 1, g + 1, springs, groups) + process_springs(p + 1, g, springs, groups)
        else:
             result = process_springs(p + 1, g, springs, groups)
    elif springs[p] == "#":
        if "." not in springs[p:p + groups[g]] and springs[p + groups[g]] != "#":
            result = process_springs(p + groups[g] + 1, g + 1, springs, groups)
        else:
            return 0
    else:
        result = process_springs(p + 1, g, springs, groups)

    return result

def main() -> None:
    test_lines = get_file_data("test1.txt")

    test_1_total = 0
    for line in test_lines:
        springs, groups = process_line(line)
        test_1_total += process_springs(0, 0, springs, groups)

    assert test_1_total == TEST_1_SOLUTION, "Test 1 generated answer {} does not match solution {}".format(test_1_total, TEST_1_SOLUTION)

    input_lines = get_file_data("input.txt")

    part_1_total = 0
    for line in input_lines:
        springs, groups = process_line(line)
        part_1_total += process_springs(0, 0, springs, groups)

    print("Part 1 solution: {}".format(part_1_total))

    test_2_total = 0
    for line in test_lines:
        springs, groups = process_line(line, True)
        test_2_total += process_springs(0, 0, springs, groups)

    assert test_2_total == TEST_2_SOLUTION, "Test 2 generated answer {} does not match solution {}".format(test_2_total, TEST_2_SOLUTION)

    part_2_total = 0
    for line in input_lines:
        springs, groups = process_line(line, True)
        part_2_total += process_springs(0, 0, springs, groups)

    print("Part 2 solution: {}". format(part_2_total))

if __name__ == '__main__':
    main()
